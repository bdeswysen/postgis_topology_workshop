# <span style="color:darkblue">Setup<span>

This workshop needs a specific setup based on a PostGIS database. Please prepare it in advance to have it up and running the D-day! 

Our Team is of course available to help you fix it in case of technical issue but this is not the main topic of the workshop.

---------------------------
## <span style="color:darkblue">PostGIS database<span>
---------------------------

To install PostGIS on your laptop, we suggest 2 approaches:
- A classical setup based on a PostgreSQL installation
- A PostGIS docker container

You are free to use the one you prefere or another, such as _OsGeo live_, if you are confortable with.

### <span style="color:darkblue">Classic installation<span>

#### <span style="color:darkblue">PostgreSQL<span>
Follow instructions provided on the official PostgreSQL website to install it on your computer:

- For windows installation:
https://www.postgresql.org/download/windows/

- For ubuntu installation:
https://www.postgresql.org/download/linux/ubuntu/



#### <span style="color:darkblue">PostGIS extension<span>

Once you have a PostgreSQL database installed, you can intall PostGIS as a PostgreSQL extension.

To do so, run the following command as user _postgres_:
```sql
CREATE EXTENSION postgis;
```

You can then check your installed version using:
```sql
SELECT Postgis_Full_Version();
```


Source: https://www.PostGIS.net/documentation/getting_started/



### <span style="color:darkblue">Docker container<span>

If you have docker installed on your laptop, you can directly run the following _docker run_ command and adapt the parameters to your desired setup:

```docker
docker run \
    --name postgis \
    -p 5432:5432 \
    -e POSTGRES_PASSWORD=postgres \
    -d postgis/postgis 
```


- ```--name postgis``` specify the name of your new docker container as "postgis"

- ```-p 5432:5432``` setup the port forwarding. The first port number correspond to your _host port_ and the second port number correspond to your _container port_. 
    - _Host port number_: by default we suppose you don't have another process running on PostgreSQL default port number, 5432. If you have another PostgreSQL running on this port, change this parameter using for example ```-p 5452:5432``` to setup your container on host port number 5452.
    - _Container port number_: 5432 is the default port of PostgreSQL and you need to expose it to be able to access the database in your container. You can keep this value even if you have another PostgreSQL installation or container.  

- ```-e POSTGRES_PASSWORD=postgres``` specify the password for admin user postgres. In this workshop we will only setup a sandbox to play with PostGIS topology and use postgres/postgres as credentials. If you consider using this container for another purpose please change this setup.

- ```-d postgis/postgis``` specify the docker image from dockerhub we use. Here we use the official PostGIS image ( https://hub.docker.com/r/postgis/postgis ) based on _postgres_ image.


---------------------------
## <span style="color:darkblue">SQL client<span>
---------------------------

> __We recommend to prepare this step before the workshop, to have your environment ready__.

To interact with your database, we recommend 2 clients applications with GUI and SQL editor: DBeaver or pgAdmin. You will need __one of them__. You could also run SQL command in _psql_ command line tool but it is less user friendly in this learning context. 

### <span style="color:darkblue">DBeaver community edition<span>
> DBeaver Community is a free cross-platform database tool for developers, database administrators, analysts, and everyone working with data. It supports all popular SQL databases like MySQL, MariaDB, PostgreSQL, SQLite, Apache Family, and more.

DBeaver _community edition_ is the software we recommend to run SQL queries during this workshop. To install it, you can follow the instructions on their official website:
https://dbeaver.io/download/

To create your first PostgreSQL connexion you can follow the documentation: https://dbeaver.com/2022/03/03/how-to-create-database-connection-in-dbeaver/ . 
Note that DBeaver will automatically download and install PostgreSQL driver when you will create this first PostgreSQL connexion. 

### <span style="color:darkblue">pgAdmin<span>
> pgAdmin is the most popular and feature rich Open Source administration and development platform for PostgreSQL, the most advanced Open Source database in the world. 

If you prefer official PostgreSQL client and interface, you can also install and use pgAdmin: https://www.pgadmin.org/download/

---------------------------
## <span style="color:darkblue">QGIS<span>
---------------------------
> A Free and Open Source Geographic Information System 

What would be an OsGeo workshop without a map?! To browse the geographic data we will use QGIS and the "TopoViewer" embedded in the DB Manager. 

To install it, please follow official documentation:
https://www.qgis.org/en/site/forusers/download.html


