# <span style="color:darkblue">Prepare data<span>

---------------------------
## <span style="color:darkblue">Download<span>
---------------------------

Download of the [belgian administrative units on geo.be](https://www.geo.be/catalog/details/629ad470-71dc-11eb-af47-3448ed25ad7c?l=en) open data portal in Belgian Lambert 2008 coordinate system:
https://opendata.fin.belgium.be/download/datasets/AU-RefSit_20220101_shp_3812_01000.zip


Here is a description directly coming from the [layer metadata](https://www.geo.be/metadataServices/rest/catalogs/1/resources/629ad470-71dc-11eb-af47-3448ed25ad7c/xml):

> Administrative Units - situation on January 1st is extracted from the reference database for the land register data, CadGIS, whose situation is fixed on January 1st of the reference year. The geometric representation of administrative limits published in CadGIS is made up of a set of sections whose layout (X and Y coordinates) corresponds to the situation of an administrative limits at a given moment. Sometimes the coordinates are legally recorded, but often they are coming from an analysis and interpretation of legal documents, and/or field data or any other relevant information such as regional reference frames, aerial photographs or original plans. Therefore, this layout does not possess the same quality everywhere. Especially in places that have not been subject to recent measurements, (carto)graphic coordinates can be proposed and can therefore change over time if new information of better quality is provided in compliance with the update rules currently being developed. In the CadGIS database, the administrative limits are the delimitations of Belgian administrative entities at different hierarchical levels, i.e. municipalities, administrative arrondissements, provinces, regions and finally the country. Polygons (corresponding to the territories of the different hierarchical levels from the municipality to the country) are generated on the basis of the layout of these limits.

In this workshop we will focus on the municipalities (_Apn_AdMu_) shapefile layer. Here is a preview:

```{image} ./figures/municipalities.png
:alt: municipalities
:width: 500px
:align: center
```

---------------------------
## <span style="color:darkblue">Load municipalities in PostGIS<span>
---------------------------


Although we could fill the ```topo_data.municipality``` table directly from the source file (AdMu shapefile), here we favor the use of an intermediate staging table: ```topo_data.Apn_AdMu```.

This intermediate step allows us to perform some quality checks before feeding our final ```topo_data.municipality``` table.

### <span style="color:darkblue">Load data using QGIS DB Manager<span>

_QGIS > Database > DB Manager > [your db connection] > Import Layer/File_ :


```{image} ./figures/import_admu_from_qgis.png
:alt: import_municipalities
:width: 500px
:align: center
```

### <span style="color:darkblue">Optional step: data quality check<span>

Once the shapefile has been loaded into PostGIS, we can run some optional tests about data quality.
It is a good practice to check data quality before going further to avoid unecessary steps and having to go back because of poor data quality.

```{note}
In this workshop, the data is of very good quality but you know as I do that this is not always the case.
The following steps are therefore optional - as they will not detect any major default in the data - but are provided to allow you to transpose this exercise to other datasets.
```

#### <span style="color:darkblue">Preview in DBeaver or pgAdmin<span>

Run some _select_ queries and browse data to check that text encoding is correctly setted. 

```sql
-- select to check data visually
select *
from topo_data."Apn_AdMu" a ;
```
For example, if you tried to load this shapefile with _utf-8_ encoding for text fields, you probably have some special caracter wrongly encoded:

```{image} ./figures/utf8_encoding.png
:alt: utf8_encoding
:width: 300px
:align: center
```


#### <span style="color:darkblue">Preview in QGIS<span>

Load ```topo_data."Apn_AdMu"``` PostGIS table as QGIS layer.
All the geometries should appear. 

You can for example 
- try to apply a symbology with transparency setted to 50%. If your geometries overlap, you can detect it visually. In this case, you will need to find a way for cleaning them.

- use an OpenStreetMap basemap to check that your layer coordinates system is correctly defined. If it is correctly defined, they should be aligned.

#### <span style="color:darkblue">Number of municipalities<span>

According to Statbel ( https://statbel.fgov.be/fr/propos-de-statbel/methodologie/classifications/geographie ), we should have 581 municipalities in the table.
Let's check it in SQL:
```sql
-- check number of municipalities
select count(*)
from topo_data."Apn_AdMu" a ;
```


#### <span style="color:darkblue">Municipalities without name<span>

Check if all your records have a name:
```sql
-- check name of municipalities
select *
from topo_data."Apn_AdMu" a
where namefre is null;
```


#### <span style="color:darkblue">Detect null, empty or invalid geometries<span>

Each format has its own way of storing and defining geometries. 
Moving from one format to another can result in null, empty or invalid geometry in the destination:

```sql
-- check null geom
select a.namefre, 'NULL geom' observation
from topo_data."Apn_AdMu" a
where geom is null
union all
-- check empty geom
select a.namefre, 'EMPTY geom' observation
from topo_data."Apn_AdMu" a
where st_isempty(geom) 
union all
-- check invalid geom
select a.namefre, 'INVALID geom:' || st_isvalidreason(geom)  observation
from topo_data."Apn_AdMu" a
where st_isvalid(geom) is false ;
```





---------------------------
## <span style="color:darkblue">Transfer to destination table<span>
---------------------------

Once all the quality checks have been successful, you can transfer the municipalities data to 
```topo_data.municipality``` destination table:

```sql
-- insert from staging table into topo_data.municipality
insert into topo_data.municipality(name, adrekey, geom)
select namefre, adrekey, geom 
from topo_data."Apn_AdMu" a 
order by id asc;
```


```{note}
Note that we order them by id to keep the same id order as the source. ID column is a _serial_ and use then an auto-incremented sequence starting from 1, like technical IDs in the original table.
```

---------------------------
## <span style="color:darkblue">Optional: clean data using Mapshaper<span>
---------------------------

[Mapshaper](https://mapshaper.org/) is a software for editing Shapefile, GeoJSON, TopoJSON, CSV and several other data formats, written in JavaScript. 
This software has it's own internal topological format and is able to perform topologically consistent operations such as simplification, snapping or cleaning. 
It also supports other map making tasks like editing attribute data, clipping, erasing, dissolving, filtering and more.


To clean your data, you can use Mapshaper and just need to drag'n'drop your shapefile files (sorry, geopackage is not supported :-/ ) in Mapshaper web interface: https://mapshaper.org/ and select "detect line intersections" and/or "snap vertices".


```{image} ./figures/mapshaper_screenshot.png
:alt: mapshaper_screenshot
:width: 500px
:align: center
```

Note that a command line version, with more options, is also available. You can then combine it with other tools such as _ogr2ogr_ (GDAL), linux _bash_ command line, _python_ scripts, etc. and make your own home made open source ETL!


```{note}
EXERCISE: Try Mapshaper online version: 
- detect line intersections
- snap vertices
- try simplification algorithms (_Simplify_ menu at the top right)
```