# <span style="color:darkblue">Introduction<span>
    


Welcome to the world of PostGIS topology! In this comprehensive workshop, we will take you through a step-by-step journey to master the utilization of PostGIS topology for managing and analyzing spatial data.

---------------------------
## <span style="color:darkblue">Setup<span>
---------------------------


In this first chapter, we will guide you through the initial setup process, ensuring that you have the necessary tools and environment to work with PostGIS topology. We will cover the installation of PostgreSQL, PostGIS, and other essential tools such as QGIS and DBeaver, enabling you to create an efficient database environment.

---------------------------
## <span style="color:darkblue">Prepare Topology<span>
---------------------------

In the second chapter, we will delve into the process of preparing your topology environment. 
You will learn how to create a topological framework to include nodes, edges, faces and relationships between them.
The DDL structure of database tables and spatial indexes is also covered in this part.

---------------------------
## <span style="color:darkblue">Prepare Data<span>
---------------------------
In the third chapter, we will focus on preparing your spatial data for topological analysis. We will cover topics such as data download, exploration, vizualisation, quality checks and cleaning.

---------------------------
## <span style="color:darkblue">Convert to Topology<span>
---------------------------
In the fourth chapter, we will guide you through the process of converting your spatial data into a topological representation using PostGIS. Some custom functions will be defined to help you in your journey. This chapter will provide you with the necessary knowledge and tools to transform your spatial data into a topological structure.

---------------------------
## <span style="color:darkblue">Simplification<span>
---------------------------

In the fifth chapter, we will explore the simplification techniques available in PostGIS topology. Simplification allows you to reduce the complexity of your dataset while preserving important spatial relationships. We will try differents algorithms and approaches for simplifying your geographic data.

---------------------------
## <span style="color:darkblue">Going further<span>
---------------------------
In the final chapter, we will mention advanced topics and techniques and provide a support for open discussions for the drink following this workshop! ;-) 

---------------------------
## <span style="color:darkblue">Take away<span>
---------------------------

Throughout this workshop, we will provide hands-on exercises and practical examples to reinforce your understanding of PostGIS topology concepts and techniques. So, get ready to embark on this exciting journey and unlock the power of PostGIS topology for your spatial data management and analysis needs. Let's dive in!

---------------------------
## <span style="color:darkblue">Additional ressources<span>
---------------------------

- Official postgis topology documentation: https://postgis.net/docs/Topology.html
- "Postgis in action" book - Regina OBE: https://www.manning.com/books/postgis-in-action-third-edition 