# <span style="color:darkblue">Validate topology<span>

Checking the topological validity of data in PostGIS topology ensures that spatial relationships between features, such as adjacency and containment, are correctly maintained without overlaps or gaps. This validation is crucial for maintaining the integrity of complex spatial datasets, preventing errors in analysis and data processing. Additionally, it ensures that any spatial operations or queries performed on the data produce accurate and reliable results

---------------------------
## <span style="color:darkblue">Validate topology with postgis<span>
---------------------------


### <span style="color:darkblue">Validate_topology() function<span>


Postgis offers a dedicated function to identify topological invalidities: 
[ValidateTopology()](https://postgis.net/docs/ValidateTopology.html).
And since postgis 3.5 this function is also available in a specific research area defined by a bounding box geometry. This last feature is particularly useful when updating a specific zone in the database: you don't have to validate the entire dataset.

The following topological incoherences are identified by the [ValidateTopology()](https://postgis.net/docs/ValidateTopology.html) function:

| Error code | Meaning |
|---|---|
| coincident nodes | Two nodes have the same geometry. |
| edge crosses node | An edge has a node in its interior. See [ST_Relate](https://postgis.net/docs/ST_Relate.html). |
| invalid edge | An edge geometry is invalid. See  [ST_IsValid](https://postgis.net/docs/ST_IsValid.html). |
| edge not simple | An edge geometry has self-intersections. See  [ST_IsSimple](https://postgis.net/docs/ST_IsSimple.html). |
| edge crosses edge | Two edges have an interior intersection. See  [ST_Relate](https://postgis.net/docs/ST_Relate.html). |
| edge start node geometry mis-match | The geometry of the node indicated as the starting node for an edge does not match the first point of the edge geometry. See  [ST_StartPoint](https://postgis.net/docs/ST_StartPoint.html). |
| edge end node geometry mis-match | The geometry of the node indicated as the ending node for an edge does not match the last point of the edge geometry. See  [ST_EndPoint](https://postgis.net/docs/ST_EndPoint.html). |
| face without edges | No edge reports an existing face on either of its sides (left_face, right_face). |
| face has no rings | Edges reporting a face on their sides do not form a ring. |
| face has wrong mbr | Minimum bounding rectangle of a face does not match minimum bounding box of the collection of edges reporting the face on their sides. |
| hole not in advertised face | A ring of edges reporting a face on its exterior is contained in different face. |
| not-isolated node has not- containing_face | A node which is reported as being on the boundary of one or more edges is indicating a containing face. |
| isolated node has  containing_face | A node which is not reported as being on the boundary of any edges is lacking the indication of a containing face. |
| isolated node has wrong containing_face | A node which is not reported as being on the boundary of any edges indicates a containing face which is not the actual face containing it. See  [GetFaceContainingPoint](https://postgis.net/docs/GetFaceContainingPoint.html). |
| invalid next_right_edge | The edge indicated as the next edge encountered walking on the right side of an edge is wrong. |
| invalid next_left_edge | The edge indicated as the next edge encountered walking on the left side of an edge is wrong. |
| mixed face labeling in ring | Edges in a ring indicate conflicting faces on the walking side. This is also known as a "Side Location Conflict". |
| non-closed ring | A ring of edges formed by following next_left_edge/next_right_edge attributes starts and ends on different nodes. |
| face has multiple shells | More than a one ring of edges indicate the same face on its interior. |


### <span style="color:darkblue">Run validate_topology() function to our data<span>


We can try to run the following query to detect errors in our topology layer:

```sql
-- validate entire topology
SELECT topology.validatetopology('topo');
```

You should see the following logs in your console when the function is running:
```
Checking for coincident nodes
Checking for edges crossing nodes
Checking for invalid or not-simple edges
Checking for crossing edges
Checking for edges start_node mismatch
Checking for edges end_node mismatch
Checking for faces without edges
Checking edge linking
Building edge rings
Found 681 rings, 648 valid shells, 33 valid holes
Constructing geometry of all faces
Checking faces
Checked 648 faces
Checking for holes coverage
Finished checking for coverage of 33 holes
Checking for node containing_face correctness
```
If you didn't modify your municipalities dataset, you should not get any error in return.


```{note}
EXERCISE: 
Try to create an error in your topology:
 1. Modifying the primitives (nodes, edges, faces) to create an incoherence
 2. Run the _validatetopology()_ function
```

---------------------------
## <span style="color:darkblue">Validate topology with QGIS<span>
---------------------------

QGIS also has its own tool to detect topological incoherences: the [Topology Checker Plugin](https://docs.qgis.org/3.34/en/docs/user_manual/plugins/core_plugins/plugins_topology_checker.html#index-0).


> With the Topology Checker plugin, you can look over your vector files and check the topology with several topology rules. These rules check with spatial relations whether your features ‘Equal’, ‘Contain’, ‘Cover’, are ‘CoveredBy’, ‘Cross’, are ‘Disjoint’, ‘Intersect’, ‘Overlap’, ‘Touch’ or are ‘Within’ each other.

To find the plugin in QGIS, go to
_QGIS > Vector > Topology checker_. You should now see the following toolbox:

```{image} ./figures/qgis_topology_checker.png
:alt: utf8_encoding
:width: 300px
:align: center
```
You can now go to the _configure_ option and create a new topology rule to detect the gaps between polygons:

```{image} ./figures/qgis_topology_checker_rule.png
:alt: utf8_encoding
:width: 300px
:align: center
```

And then you can apply it on this layer (small check icon in the toolbox) and investigate the potential error detected.


```{note}
EXERCISE: 
create and combine other topology rules in QGIS topology checker
```


