# <span style="color:darkblue">Hierarchical topologies<span>


Another advantage of using PostGIS topology is to build hierarchical topologies that are also often called "nested boundaries". 

The underlying idea is to aggregate existing topogeometries together and use the same topologic primitives (edges, nodes and faces) to build hierarchical levels. With that approach, the geometric union will fit perfectly between all levels and they will stay delimited by the exact same borders:

```{image} ./figures/boundaries_illustration.jpg
:alt: boundaries_illustration
:width: 500px
:align: center
```



---------------------------
## <span style="color:darkblue">Hierarchical topologies in Postgis<span>
---------------------------
Postgis allows to define different hierarchical layers in the same topology.
If you remember well, we previously created a single _municipality_ layer associated to our topology (called "topo") in the __Prepare database__ section:

```{image} ./figures/datamodel_topology.png
:alt: utf8_encoding
:width: 600px
:align: center
```

We will now create a _region_ layer which will use the municipality layer as child:

```{image} ./figures/datamodel_topology_with_child.png
:alt: utf8_encoding
:width: 600px
:align: center
```

This new layer will not have its own topologic primitives but will contain references to existing ones from the child layer.

### <span style="color:darkblue">Create new region table<span>

Before creating the _region_ topology layer, we will create a ```topo_data.region``` table to receive this data:

```sql
-- region 
CREATE TABLE IF NOT EXISTS topo_data.region (	
    id serial primary KEY,
    adrekey varchar(5),
    name varchar(100)
);
```

We can already add a geometry column and a spatial index:

```sql
-- geom column
SELECT AddGeometryColumn ('topo_data','region','geom',3812,'MULTIPOLYGON',2);

-- spatial index
CREATE INDEX region_sidx ON topo_data.region USING gist(geom);
```




### <span style="color:darkblue">Create corresponding topology layer<span>

To create this new topology layer, we will use again the [AddTopoGeometryColumn()](https://postgis.net/docs/AddTopoGeometryColumn.html) function. To get the correct layer_id of the existing child table, we will combine it with a specific ```SELECT``` query:

```sql
-- topogeometry column
select topology.AddTopoGeometryColumn( 'topo', 'topo_data', 'region', 'topo', 'POLYGON',child.layer_id)
from topology.layer child
where child.schema_name = 'topo_data' and child.table_name='municipality';
```

We can now see our new layer defined in the ```topology.layer``` table.
We see that our level 0 is the municipality layer and the level 1 is the region layer. 



```{note}
EXERCISE: 
Think about how you could implement it for all the country level. For example, in Belgium, admin levels are: _Municipality_ > _District_ > _Province_ > _Region_ > _Country_.

How would you define the parenthood relationship?
Be aware that there may be gaps in the hierarchy: as Belgium appreciates exceptions, there is no province of Brussels (!). Be careful not to create any gaps where the relationship would be lost.
Click when you are ready to discover the hint.
:::{toggle} solution
You need to define all of them as parent of the municipality layer and avoid building a pyramid.
Otherwise, you would not be able to skip "gaps" in the admin hierarchy.
:::
```
### <span style="color:darkblue">Create regions from municipalities<span>

Before creating the regions based on the ```adrekey``` attribute we kept in the previous steps, we will evaluate their distribution with a _group by_ operation:

```sql
-- evaluate distribution per region
select count(*), adrekey
from topo_data.municipality m 
group by adrekey
order by adrekey;
```

You can quite easily guess the 3 regions (if, like me, you are from Belgium ;-) ): 
- region ```02000``` corresponds to Flanders with 300 municipalities
- region ```03000``` corresponds to Wallonia with 262 municipalities
- region ```04000``` corresponds to Brussels with 19 municipalities


```{image} ./figures/municipalities_grouped_by_region.png
:alt: utf8_encoding
:width: 300px
:align: center
```

We can now insert the regions and build their topogeometry based on their parent topogeometry. If we take the example of Brussels region, we wan use the following SQL statement:


```sql
INSERT INTO topo_data.region( adrekey, topo)
SELECT 
	'04000' as adrekey,
	topology.createTopoGeom(
	'topo', 				-- toponame
	3, 						-- geom type: 3=polygon
	parent.layer_id, 		-- current layer id -> "parent" layer
	( 	SELECT array_agg(ARRAY[(a.topo).id, parent.layer_id])
		FROM topo_data.municipality a, topology.layer parent
		where parent.table_name = 'municipality'
			and a.adrekey = '04000'))	 as  topo		-- topo = aggregate of parent existing topo elements
FROM topology.layer parent
WHERE parent.table_name = 'region'	;
```

You can note that we build the topogeometry object by aggregating its ```municipality``` parents in list of IDs (```array_agg()``` function) associated to the new ```region``` _layer_id_. 
Postgis will then directly get the references to the topologic primitives we previously inserted.


If we want to display this entity as geometry, we need to convert (="cast") it to geometry and we can use the following SQL command:

```sql
UPDATE topo_data.region 
SET geom = topo::geometry 
WHERE adrekey='04000';
```

You can then load it in QGIS as vector layer to check if the results are coherent.


### <span style="color:darkblue"> Automate regions creation<span>

Now that we found a way to create a region topogeometry and the resulting geometry based on its children (municipalities), we can easily automate the process in a loop (after removing region records previously inserted).

:::{toggle} generate_regions
```sql
do $$
declare 
	v_parent 		varchar := 'municipality';
	v_child 		varchar := 'region';
	rec_region 		record;
begin 
	
	raise notice 'Convert % topo from % parent layer.', v_child, v_parent;

	-- loop on region
	for rec_region in
		select distinct adrekey
		from topo_data.municipality
	loop
		
		raise notice ' - Compute region %', rec_region.adrekey;

		-- insert region + topo 
		INSERT INTO topo_data.region( adrekey, topo)
		select rec_region.adrekey as adrekey,
			topology.createTopoGeom(
			'topo', 				-- toponame
			3, 						-- geom type: 3=polygon
			parent.layer_id, 		-- current layer id -> "parent" layer
			( 	SELECT array_agg(ARRAY[(a.topo).id, parent.layer_id])
				FROM topo_data.municipality a, topology.layer parent
				where parent.table_name = 'municipality'
					and a.adrekey = rec_region.adrekey))	 as  topo		-- topo = aggregate of parent existing topo elements
		FROM topology.layer parent
		WHERE parent.table_name = 'region'	;
	
	end loop;
	
	raise notice 'Update geometries.';

	-- compute geometry
	update topo_data.region set geom = topo::geometry;
end;
$$
```
:::


We can now set the region names with simple update statements:
```sql
update topo_data.region set name='Brussels' where adrekey='04000';
update topo_data.region set name='Flanders' where adrekey='02000';     
update topo_data.region set name='Wallonia' where adrekey='03000';   
```
 

 ```{note}
EXERCISES: 
- Implement the same logic for different admin levels: district, provinces, country, ...
- Compare your result with official datasets such as AdRe from geo.be
```

