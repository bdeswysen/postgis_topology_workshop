# Postgis topology workshop

Welcome to the world of PostGIS topology! In this comprehensive workshop, we will take you through a step-by-step journey to master the utilization of PostGIS topology for managing and analyzing spatial data.

You can access the book here: https://bdeswysen.gitlab.io/postgis_topology_workshop

Feel free to share any feedback