# <span style="color:darkblue">Convert to PostGIS topology<span>


---------------------------
## <span style="color:darkblue">Topology functions<span>
---------------------------


### <span style="color:darkblue">Add topo<span>

This small function will be usefull to convert an existing _geometry_ to its _topogemetry_ equivalent in ```topo_data.municipality``` table. 
This operation will fill the _topo_ column based on the _geometry_ column for a specified _id_.

This function relies on _topology.toTopoGeom()_ PostGIS function but implement some additional features such as embedded quality and coherence checks:


:::{toggle} add_municipality function
```sql
-- create function to convert municipality geom > topogeometry. 
-- Needs municipality ID as parameter. Default tolerance is set to 1 meter by default.
CREATE OR REPLACE FUNCTION topo_data.add_municipality(p_id integer, p_tolerance integer DEFAULT 1)
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$
DECLARE

begin
	case 
		-- check if municipality doesn't exist with this id
		when not exists (SELECT 1 FROM topo_data.municipality WHERE id=p_id) then 
			raise notice 'Municipality (%) doesnt exist.', p_id;
			RETURN FALSE;
	
		-- check if municipality is already in topo
		when EXISTS (SELECT 1 FROM topo_data.municipality WHERE id=p_id AND topo is not null) THEN
			raise notice 'Municipality (%) already exists in topo. Remove it before adding if needed.', p_id;
			RETURN FALSE;
	
		-- check for null or empty geometry
		when exists (SELECT 1 FROM topo_data.municipality WHERE id=p_id AND (geom is null or st_isempty(geom))) then
			raise notice 'Municipality (%) geom is null or empty.', p_id;
			RETURN FALSE;
	
		-- check for invalid geometry
		when exists (SELECT 1 FROM topo_data.municipality WHERE id=p_id and st_isvalid(geom) is false  ) then
			raise notice 'Municipality (%) geom is null or empty.', p_id;
			RETURN FALSE;
		
		else
			-- convert geom into topo 
			update topo_data.municipality m
			set topo = topology.toTopoGeom(geom, 'topo',l.layer_id , p_tolerance)
			from topology.layer l 
			where m.id = p_id and l.table_name='municipality' and l.schema_name='topo_data';

			-- succes: return true
			return true;
			
	end case;

	-- catch any kind of exception: print error message + return false
	EXCEPTION WHEN OTHERS THEN
		RAISE notice 'ERROR: %', SQLERRM;
		return false;
	
END;
$function$
;
```
:::


### <span style="color:darkblue">Reset topo<span>

To simplify operation during this workshop, we will not delete the topogeometry objects (and the associated topological primitives) one by one. Indeed, the relational data model implemented for topological primitives requires complex cascading deletions. When modifying a part of the topology, the neighborhood must be reassigned correctly in order to avoid creating orphan data.

To avoid this we will simplify the process and delete __all__ contents associated with topological data within ```topo_data.reset_topo()``` function. This function will remove:
- _topogeometry_ objects (= topo column)
- topological primitives: _edges_, _nodes_, _faces_ and _relations_.

This approach is acceptable in the context of this workshop as it only takes few minuts to re-generate municipalities topologies and it helps to restart from a clean setup between your tests.


:::{toggle} reset_topo function
```sql
CREATE OR REPLACE FUNCTION topo_data.reset_topo_data( )
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$
DECLARE

begin

	raise notice 'Reset topo:';

	-- remove data in table with topogeometry column
	raise notice '- Remove topo_data.municipality(topo).';

	-- reset topo column as NULL
	update topo_data.municipality 
	set topo = null;

	-- remove relations
	raise notice '- Remove relations.';
	DELETE FROM topo.relation;

	-- remove edges
	raise notice '- Remove edges.';
	perform topology.ST_RemEdgeModFace('topo', edge_id) FROM topo.edge_data;

	-- remove nodes
	raise notice '- Remove nodes.';
	perform topology.ST_RemoveIsoNode('topo', node_id) FROM topo.node;

	return true;

	exception when others then
		RAISE notice 'Unable to reset topo. Details: %',SQLERRM;
		return false;
END;
$function$ ;
```
:::


```{warning}
 <span style="color:red">__Only run this reset function if you are sure you want to delete all your topology primitives.__<span>
```

Once it has been created, you can call this function by running:

```sql
-- reset 'topo' topology
select topo_data.reset_topo_data();
```



---------------------------
## <span style="color:darkblue">"Hello world": create your first topogeometry<span>
---------------------------

```sql
-- add muncipality id 1 as topology
select id, name, topo_data.add_municipality(id)
from topo_data.municipality m 
where id = 1
```

Congratulations, you have just created your first topogeometry object!


---------------------------
## <span style="color:darkblue">QGIS TopoViewer<span>
---------------------------


QGIS provides a specific Postgis topology viewer which is embedded in the _DB Manager_.
To use it, go to:

_QGIS > database > DB Manager > select and expand your db connection > select "topo" schema > "schema" menu > TopoViewer_.

It will automatically load all the Postgis topology primitives (edges, faces and nodes) as QGIS layers in a dedicated group.
__Note that they are not activated by default__. We suggest to activate the following layers to see them in your map canvas:
- _topo.face_mbr_
- _topo.node_
- _topo.edge_


Once you setted their rendering as you want: you should be able to find your first PostGIS topology insertion:

```{image} ./figures/topology_hello_world.png
:alt: topology_hello_world
:width: 500px
:align: center
```

---------------------------
## <span style="color:darkblue">Add other municipalities in topology<span>
---------------------------

We recommend to add other municipalities by several batch of 100 to avoid causing a scaling up of your local database and (too) long transactions:

```sql
-- add muncipality as topology. batch of 100
select id, name, topo_data.add_municipality(id)
from topo_data.municipality m 
where m.topo is null 
limit 100;
```

Once you have all the municipalities converted, you should see something similar to this in your QGIS environment:


```{image} ./figures/topo_full_country.png
:alt: topo_full_country
:width: 500px
:align: center
```

---------------------------
## <span style="color:darkblue">Optional: identify topologic errors<span>
---------------------------

Sometimes contigous geometries boundaries are not perfectly aligned. 
In this case, you can have topologic errors such as overlaps or gaps and they are easier to detect when your geometries have been converted into topologies:



### <span style="color:darkblue">Overlaps<span>

If you set some transparency to your QGIS polygon layers, you can identify overlap between contigous poplygons because they appear as darker areas:

```{image} ./figures/overlap_1.png
:alt: overlap
:width: 500px
:align: center
```

In topology primitives, you can see that PostGIS added an extra face around this overlap:
```{image} ./figures/overlap_2.png
:alt: overlap
:width: 500px
:align: center
```

You can try to identify these gaps by searching for "small" faces. The meaning of small depends of your spatial context. All small faces are not necessarlly topologic errors!





### <span style="color:darkblue">Gaps<span>

You can directly see that an extra _face_ has been created around the gap between adjacent polygons. You can again identify small faces to identify gaps.
An abnormally high concentration of nodes makes also it easier to detect this type of case.

```{image} ./figures/gap_example.png
:alt: gap_example
:width: 500px
:align: center
```


```{note}
EXERCISE: To understand these 2 use cases, try to reproduce them by following these steps:

- Remove topology primitives (reset function)
- Update topo_data.municipality geometry for some municipalities using QGIS (edit layer) and generate gaps and overlaps between polygons.
- Convert the geometries in topogeometry using topo_data.add_municipality() function
```
