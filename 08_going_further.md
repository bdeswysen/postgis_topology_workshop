
# <span style="color:darkblue">What's next?<span>

---------------------------
## <span style="color:darkblue">Over-simplification<span>
---------------------------

As you have seen in the previous page, a challenge you can face when using Postgis topology is the over-simplification of your "geometries" depending on the tolerance you are using:
small geometries dissappear and you don't have any geographical representation for your object at this simplification level.

---------------------------
## <span style="color:darkblue">Topojson: a topologic file format<span>
---------------------------

> **TopoJSON** is an extension of GeoJSON that encodes topology. 
Rather than representing geometries discretely, geometries in TopoJSON files are stitched together from shared line segments called *arcs*. ( https://github.com/topojson/topojson )


The advantage of using the TopoJSON file format when manipulating topologic data in GIS lies in its efficient representation and compactness. TopoJSON optimizes the storage and transmission of geographic data by encoding topology, which is the relationships between adjacent features, such as shared boundaries. Unlike traditional formats like GeoJSON or Shapefile, TopoJSON __eliminates the redundancy of shared geometry__, resulting in __significantly smaller file sizes__. This compactness reduces the computational overhead required for data processing, visualization, and analysis. Additionally, TopoJSON supports the preservation of topology during transformations, simplifications, or projections, ensuring the accuracy and integrity of the underlying geographic data. 

When you already have your geographic data stored as topologies in PostGIS, you can directly export them as topojson files using functions such as ```topology.AsTopoJSON()```.

